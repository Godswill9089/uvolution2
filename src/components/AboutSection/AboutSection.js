import React, { Fragment } from 'react'
import "./AboutSection.css";

export const AboutSection = () => {
  return (
    <Fragment>
      <section id='' className='py5 mt-40 home-1'>
        <div className='container about-fle flex flex-wrap items-center justify-center mx-auto mt-10  md:px-12 md:flex-row'>
          <div className=' lg:w-1/2'>
            <img src='/Img/About Asset.png' alt='' className='lg:w-4/5' />
          </div>
          <div className='lg:w-1/2'>
            <div>
              <img src='/Img/Shape Design (1).png' alt='' className='puzzel' />
            </div>
            <p>About UVTECH</p>
            <h1
              className='max-w-xl text-[2.9rem] leading-none font-extrabold text-center lg:text-4xl
                lg:text-left lg:leading-tight mb-5'
            >
              Who We Are
            </h1>
            <p className='leading-9'>
              Uvolution Tech Hub is a leading technology centric platform that
              offers comprehensive trainings and solutions for individuals and
              businesses seeking to excel in the ever-evolving digital
              landscape. Our mission is to empower professionals, foster
              innovation, and build a thriving community of tech enthusiasts.
            </p>
          </div>
        </div>
      </section>

      <section id='' className='py5 mt-48 home-2'>
        <div className='container about-fle flex flex-wrap items-center justify-center mx-auto mt-10  md:px-12 md:flex-row'>
          <div className='lg:w-1/2'>
            <div>
              <img src='/Img/Shape Design (1).png' alt='' className='puzzel' />
            </div>
            <p className='abt-p'>About UVTECH</p>
            <h1
              className='max-w-xl text-[2.9rem] leading-none font-extrabold text-center lg:text-4xl
                lg:text-left lg:leading-tight mb-5 abt-h1'
            >
              Who We Are
            </h1>
            <p className='leading-9 abt-des'>
              Uvolution Tech Hub is a leading technology centric platform that
              offers comprehensive trainings and solutions for individuals and
              businesses seeking to excel in the ever-evolving digital
              landscape. Our mission is to empower professionals, foster
              innovation, and build a thriving community of tech enthusiasts.
            </p>
          </div>
          <div className='lg:w-1/2'>
            <img
              src='/Img/About Asset.png'
              alt=''
              className='lg:w-4/5 abt-img'
            />
          </div>
        </div>
      </section>
    </Fragment>
  );
}
