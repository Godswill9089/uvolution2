import React, { Fragment } from 'react'
import { Navbar } from '../Navbar/Navbar'
import "./Project.css";
import { Footer } from '../Footer/Footer';

export const Project = () => {
  return (
    <Fragment>
      <Navbar />
      <div>
        <div className='items center'>
          <h1
            className='w-2/5 ml-auto mr-auto mt-40 leading-10 font-extrabold  text-center lg:text-3xl
             lg:leading-tight mb-5'
          >
            Do you want to become an effective project manager or boost your
            project leadership skills
          </h1>
          <div className='blur1 h-1'></div>
          <div className='blur2 h-1'></div>
          <div className='blur3 h-1'></div>
        </div>
        <img
          src='/Img/Mask group (5).png'
          alt=''
          className='absolute top-40 left-60 absolute-left'
        />
        <img
          src='/Img/Mask group (7).png'
          alt=''
          className='absolute top-40 left-3/4'
        />
        <img
          src='/Img/Mask group (8).png'
          alt=''
          className='absolute left-60 absolute-left'
        />
        <img
          src='/Img/Mask group (7).png'
          alt=''
          className='absolute left-1/2 mt-10 m-top'
        />
        <img
          src='/Img/Mask group (9).png'
          alt=''
          className='absolute left-3/4 ml-10 absolute-ml'
        />
      </div>

      <section id='home4' className='py5 mt-48'>
        <div className='container flex flex-wrap items-center justify-center mx-auto mt-96  md:px-12 md:flex-row'>
          <div className=' lg:w-1/2'>
            <img src='/Img/Group 37.png' alt='' className='lg:w-4/5' />
          </div>
          <div className='lg:w-1/2'>
            <div>
              <img src='/Img/Shape Design (1).png' alt='' className='puzzel' />
            </div>
            <p>UVTECH Project Management</p>
            <h1
              className='max-w-xl text-[2.9rem] leading-none font-extrabold text-center lg:text-4xl
                lg:text-left lg:leading-tight mb-5 w-4/5'
            >
              Why Choose Project Management
            </h1>
            <p className='leading-9'>
              Our Project Management Training and Mentorship Program offers a
              comprehensive learning journey that equips you with the knowledge,
              tools, and practical insights needed to excel in the dynamic world
              of project management." 
            </p>
            <div className='justify-center mt-14 lg:justify-start'>
              <button
                type='button'
                className='text-white font-medium rounded-lg px-5 py-3 text-center btn-col'
              >
                Apply Now
              </button>
            </div>
          </div>
        </div>
      </section>

      <section id='home5' className='py5 mt-48'>
        <div className='container flex flex-wrap items-center justify-center mx-auto mt-96  md:px-12 md:flex-row'>
          <div className='lg:w-1/2'>
            <div>
              <img src='/Img/Shape Design (1).png' alt='' className='puzzel' />
            </div>
            <p className='pro-p'>UVTECH Project Management</p>
            <h1
              className='max-w-xl text-[2.9rem] leading-none font-extrabold text-center lg:text-4xl
                lg:text-left lg:leading-tight mb-5 w-4/5 pro-h'
            >
              Why Choose Project Management
            </h1>
            <p className='leading-9 pt'>
              Our Project Management Training and Mentorship Program offers a
              comprehensive learning journey that equips you with the knowledge,
              tools, and practical insights needed to excel in the dynamic world
              of project management." 
            </p>
            <div className='justify-center mt-14 lg:justify-start'>
              <button
                type='button'
                className='text-white font-medium rounded-lg px-5 py-3 text-center p-b btn-col proj'
              >
                Apply Now
              </button>
            </div>
          </div>
          <div className=' lg:w-1/2'>
            <img src='/Img/Group 37.png' alt='' className='lg:w-4/5 pro-img' />
          </div>
        </div>
      </section>

      <div className='lg:w-1/2'>
        <img src='/Img/Shape Design (1).png' alt='' className='puzzel2' />
      </div>
      <div className='g-bg w-full'>
        <div className='bg mt-28 '>
          <p className='text-center pro-sub justify-center'>Expectation</p>
          <h1 className='text-center pro-sub-h1  text-[2.9rem] font-extrabold lg:text-4xl mb-5'>
            What to Expect
          </h1>
        </div>

        <div className='container flex flex-wrap items-center justify-center mx-auto mt-10  md:px-12 md:flex-row justify-between'>
          <div className='box1-bg p-10'>
            <div className='flex'>
              <div className='box1 mb-5'></div>
            </div>
            <h2 className='mb-8 font-extrabold'>Training Sessions</h2>
            <p>
              Engage in hands-on training sessions led by industry experts,
              covering essential project management concepts, methodologies, and
              best practices." 
            </p>
          </div>

          <div className='box2-bg p-10'>
            <div className='flex md:items-center justify-between'>
              <div className='box2 mb-5'></div>
            </div>
            <h2 className='mb-8 font-extrabold'>Real-world Case Studies</h2>
            <p>
              Learn from real-life project scenarios and analyse successful
              projects to gain valuable insights into effective project
              execution.
            </p>
          </div>

          <div className='box3-bg p-10'>
            <div className='flex md:items-center justify-between'>
              <div className='box3 mb-5'></div>
            </div>
            <h2 className='mb-8 font-extrabold'>Practical Assignments</h2>
            <p>
              Apply your knowledge through practical assignments and projects,
              reinforcing your skills and confidence as a project leader.
            </p>
          </div>
        </div>
      </div>

      <section id='home' className='py5 mt-48 pro-t'>
        <div className='container flex flex-wrap items-center justify-center mx-auto mt-10  md:px-12 md:flex-row'>
          <div className=' lg:w-1/2'>
            <img src='/Img/Group 36.png' alt='' className='lg:w-4/5 pro-img' />
          </div>
          <div className='lg:w-1/2'>
            <div>
              <img src='/Img/Shape Design (1).png' alt='' className='puzzel' />
            </div>
            <p className='pro-sub '>UVTECH Mentorship Program</p>
            <h1
              className='max-w-xl text-[2.9rem] leading-none font-extrabold text-center lg:text-4xl
                lg:text-left lg:leading-tight mb-5 w-4/5 pro-sub-h1'
            >
              Mentorship Program
            </h1>
            <p className='leading-9  pt'>
              As part of the program, you will have the opportunity to be
              mentored by seasoned project managers who will guide and support
              you on your journey. Our mentors bring vast industry experience
              and will provide personalized guidance to help you overcome
              challenges and achieve your project management goals.
            </p>
          </div>
        </div>
      </section>

      <div className='g-bg w-full'>
        <section className='container con flex flex-wrap items-center justify-center mx-auto mt-40  md:px-12 md:flex-row'>
          <div className='flex con2 justify-between md:items-center'>
            <div>
              <h2
                className='max-w-xl text-[2.9rem] leading-none font-extrabold text-center lg:text-4xl
                lg:text-left lg:leading-tight mb-5 w-4/5 po'
              >
                Who Should Attend
              </h2>
              <p className='w-4/5 poo'>
                Our Project Management Training and Mentorship Program is
                suitable for: 
              </p>
            </div>
            <div className='b-col md:items-center p-10'>
              <div className='flex w-full mt-5 mb-10'>
                <div className='small-box mt-3 mr-5'></div>
                <p className='leading-10'>
                  Aspiring Project Managers looking to kickstart their career in
                  IT project management. 
                </p>
              </div>
              <div className='flex w-full mb-10'>
                <div className='small-box mt-3 mr-5'></div>
                <p className='leading-10'>
                  Current Project Managers seeking to up skill and transition
                  into the Tech Industry. 
                </p>
              </div>
              <div className='flex w-full'>
                <div className='small-box mt-3 mr-5'></div>
                <p className='leading-10'>
                  Fresh Graduates, Team Leaders or Professionals interested in
                  leading projects and driving successful outcomes
                </p>
              </div>
            </div>
          </div>
        </section>
      </div>

      <section id='home' className='py5 mt-40'>
        <div className='relative'>
          <div className='container pro-fo flex flex-wrap items-center justify-center mx-auto mt-10 ml-20  md:px-12 md:flex-row'>
            <div className='blur1'></div>
            <div className=' lg:w-1/2'>
              <img src='/Img/group 1.png' alt='' className='lg:w-4/5' />
            </div>
            <div className='lg:w-1/2'>
              <div>
                <img src='/Img/Shape Design (1).png' alt='' className='puzze' />
              </div>
              <p>Project Management </p>
              <h1
                className='max-w-xl text-[2.9rem] leading-none font-extrabold text-center lg:text-4xl
                lg:text-left lg:leading-tight mb-5 po'
              >
                Enrol Now
              </h1>
              <p className='leading-9'>
                Are you ready to take your career to the next level? Are you
                eager to join a dynamic and innovative team that thrives on
                pushing boundaries and delivering exceptional results? If so
              </p>
              <div className='justify-center mt-14 lg:justify-start'>
                <button
                  type='button'
                  className='text-white font-medium rounded-lg px-5 py-3 text-center btn-col p-btn'
                >
                  Enrol Now
                </button>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </Fragment>
  );
}
