import React, { Fragment } from 'react'
import "./Question.css";


export const Question = () => {

  return (
    <Fragment>
      <div className='main-con mt-40 w-full'>
        <div className=''>
          <p className='text-center que-p'>Frequent Asked Questions</p>
          <h1
            className='text-[2.9rem] leading-none font-extrabold text-center lg:text-4xl
                lg:leading-tight mb-5 que-h1'
          >
            Questions And Answer
          </h1>
        </div>
        <div className='p-20 q'>
          <div className='flex justify-evenly mx-auto que-del'>
            <div className='mb-5 card-col mx-auto container items-center justify-center mx-20 ml-0  md:px-12 md:flex-row'>
              <h2 className='mb-10 font-extrabold'>What is UvTech Hub?</h2>
              <p className='leading-10'>
                UvTech Hub is a leading technology-focused platform that offers
                skill training, outsourcing solutions, and a vast resource
                library. We empower individuals and businesses with cutting-edge
                technology knowledge and services to thrive in the digital
                age." 
              </p>
            </div>
            <div className=' mb-5 card-col2 mx-auto container items-center justify-center mx-20 ml-5  md:px-12 md:flex-row'>
              <h2 className='mb-10 font-extrabold'>
                Who can benefit from UvTech services?
              </h2>
              <p className='leading-10'>
                Whether you are an aspiring tech professional, a business
                seeking technology solutions, or a tech enthusiast eager to
                learn and collaborate, Uvolution Tech Hub has something valuable
                for you." 
              </p>
            </div>
          </div>
          <div className='flex justify-evenly mx-auto que-del'>
            <div className='card-col3 mx-auto container items-center justify-center mx-20 ml-  md:px-12 md:flex-row'>
              <h2 className='mb-10 font-extrabold'>
                How can I enrol in a training program?
              </h2>
              <p className='leading-10'>
                Enrolling is easy! Simply visit our website, navigate to the
                Training Programs page, select your desired course, and follow
                the enrolment instructions. If you have any questions, our
                support team is here to assist you
              </p>
            </div>
            <div className='card-col4 mx-auto container items-center justify-center mx-20 ml-5  md:px-12 md:flex-row'>
              <h2 className='mb-10 font-extrabold'>
                Are the training programs led by industry experts?
              </h2>
              <p className='leading-10'>
                Absolutely! Our training programs are conducted by experienced
                industry professionals who bring real-world insights and
                expertise to the classroom. You'll learn from the best in the
                field
              </p>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}
