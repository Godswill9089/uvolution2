import React, { Fragment } from 'react';
import './Services.css';
import { BiChevronRight } from 'react-icons/bi';

export const Services = () => {
  return (
    <Fragment>
      <div className='lg:w-1/2'>
        <img src='/Img/Shape Design (1).png' alt='' className='puzzel2' />
      </div>
      <div className='g-bg w-full'>
        <div className='bg mt-40'>
          <p className='text-center sev-h1 justify-center'>UVTECH Services</p>
          <h1 className='text-center sev-h1 sev-h1-sub  text-[2.9rem] font-extrabold lg:text-4xl mb-5'>
            What We Offer
          </h1>
        </div>
        <div className='bg'>
          <div className='pd flex flex-wrap sev-con items-center justify-center mx-auto mt-10  md:px-12 md:flex-row justify-between'>
            <div className='mx-auto bg-col'>
              <h1 className='font-semibold'>
                <span className='text-col'>Outsource</span> Your Technology
                Projects
              </h1>
              <p
                className='w-96 font-light mt-10 w-p Class
Properties
leading-3	line-height: .75rem; /* 12px */
leading-4	line-height: 1rem; /* 16px */
leading-5	line-height: 1.25rem; /* 20px */
leading-6	line-height: 1.5rem; /* 24px */
leading-7	line-height: 1.75rem; /* 28px */
leading-8'
              >
                Maximize efficiency and unleash innovation by leveraging our
                outsourcing solutions.
              </p>
              <div className='flex justify-between'>
                <a href='!#' className='flex items-center mt-20'>
                  Learn More <BiChevronRight className='items-center mt-1' />
                </a>
                <div>
                  <img
                    src='/Img/la_users-cog (1).png'
                    alt=''
                    className='lg:w-40'
                  />
                </div>
              </div>
            </div>

            <div className=' bg-col2 mx-auto'>
              <div className=''>
                <h1 className='font-semibold'>
                  Explore Our <span className='text-col'>Training </span>
                  Programs
                </h1>
                <p
                  className='w-96 w-p font-light mt-10 Class
Properties
leading-3	line-height: .75rem; /* 12px */
leading-4	line-height: 1rem; /* 16px */
leading-5	line-height: 1.25rem; /* 20px */
leading-6	line-height: 1.5rem; /* 24px */
leading-7	line-height: 1.75rem; /* 28px */
leading-8'
                >
                  Unlock your potential with our wide range of technology skill
                  training programs
                </p>
                <div className='flex justify-between w-p'>
                  <a href='!#' className='flex items-center mt-20 bx'>
                    Learn More <BiChevronRight className='items-center mt-1' />
                  </a>
                  <div>
                    <img
                      src='/Img/solar_laptop-outline (1).png'
                      alt=''
                      className='lg:w-40'
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};
