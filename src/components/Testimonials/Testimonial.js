import React, { Fragment, useEffect, useState } from 'react';
import './Testimonial.css';
import { BiChevronLeft, BiChevronRight } from 'react-icons/bi';
import AOS from 'aos';
import 'aos/dist/aos.css';

export const Testimonial = () => {

  const [activeTab, setActiveTab] = useState(1);

  const handleClick = (e) => {
    if (e.currentTarget.classList.contains('prev')) {
      console.log('prev');
      if (activeTab === 1) {
        return;
      }
      setActiveTab((prevState) => prevState - 1);
    } else if (e.currentTarget.classList.contains('next')) {
      if (activeTab === 3) {
        return;
      }
      setActiveTab((prevState) => prevState + 1);
    }
  };
  console.log(activeTab);

  useEffect(() => {
    AOS.init({ duration: 2000 });
  }, []);

  return (
    <Fragment>
      <div className='mt-40 small-top'>
        <div className=' mx-auto  md:px-28'>
          <div>
            <img src='/Img/Shape Design (1).png' alt='' className='puzzel3' />
          </div>
          <p className=' lg:text-left text-p'>What They Say About Us</p>
          <h1
            className='font-extrabold max-w-xl text-[2.9rem] leading-none font-extrabold text-center lg:text-4xl
                lg:text-left lg:leading-tight mb-5 text-p text-h'
          >
            Testimonials
          </h1>
        </div>

        <div className='carousel relative mx-auto justify-center ml-28 w-11/12'>
          {activeTab === 1 && (
            <div className={`cards flex gap-x-20 `}>
              <img
                src='/Img/girl.png'
                alt=''
                className='w-96 h'
                data-aos='zoom-in'
              />
              <div className='info-con'>
                <p className='w-3/4 leading-9 ml-1 test-des' data-aos='zoom-in'>
                  I am excited to share our incredible experience with
                  Uvolution. When we were faced with challenges in our business
                  operations, we knew we needed a trusted partner to help us
                  evolve and overcome these obstacles. That's when we found
                  Uvolution, and it turned out to be a game-changer for us
                </p>
                <h2
                  className='mt-20 ml-1 items-center font-extrabold   leading-none text-center lg:text-2xl
                lg:text-left lg:leading-tight mb-5 name'
                >
                  Mrs Vanessa David
                </h2>
              </div>
            </div>
          )}
          {activeTab === 2 && (
            <div className='cards flex gap-x-20'>
              <img
                src='/Img/girl 2.png'
                alt=''
                className='w-2/3 h'
                data-aos='zoom-in'
              />
              <div className='info-con'>
                <p className='w-3/4 leading-9 test-des' data-aos='zoom-in'>
                  I am excited to share our incredible experience with
                  Uvolution. When we were faced with challenges in our business
                  operations, we knew we needed a trusted partner to help us
                  evolve and overcome these obstacles. That's when we found
                  Uvolution, and it turned out to be a game-changer for us
                </p>
                <h2
                  className='mt-20 items-center font-extrabold max-w-xl text-[2.9rem] leading-none font-extrabold text-center lg:text-2xl
                lg:text-left lg:leading-tight mb-5 name'
                >
                  Mrs Vanessa David
                </h2>
              </div>
            </div>
          )}
          {activeTab === 3 && (
            <div className='cards flex gap-x-16 ml-2'>
              <img
                src='/Img/girl 3.png'
                alt=''
                className='w-96 h'
                data-aos='zoom-in'
              />
              <div className='info-con'>
                <p className='w-3/4 leading-9 test-des' data-aos='zoom-in'>
                  I am excited to share our incredible experience with
                  Uvolution. When we were faced with challenges in our business
                  operations, we knew we needed a trusted partner to help us
                  evolve and overcome these obstacles. That's when we found
                  Uvolution, and it turned out to be a game-changer for us
                </p>
                <h2
                  className='mt-20  items-center mt-20 items-center font-extrabold max-w-xl text-[2.9rem] leading-none font-extrabold text-center lg:text-2xl
                lg:text-left lg:leading-tight mb-5 name'
                >
                  Mrs Vanessa David
                </h2>
              </div>
            </div>
          )}

          <div className='btn-con absolute  right-0 bottom-28 right-5  flex justify-between mt-20'>
            <div className='flex b gap-x-4'>
              <div
                className={`rounded-full h-5 w-5 ${
                  activeTab === 1 ? 'bg-cyan-600' : 'bg-slate-300'
                }`}
              ></div>
              <div
                className={`rounded-full h-5 w-5 ${
                  activeTab === 2 ? 'bg-cyan-600' : 'bg-slate-300'
                }`}
              ></div>
              <div
                className={`rounded-full h-5 w-5 ${
                  activeTab === 3 ? 'bg-cyan-600' : 'bg-slate-300'
                }`}
              ></div>
            </div>
            <div className='btn flex gap-x-4 mr-48'>
              <button
                onClick={(e) => handleClick(e)}
                className={`prev h-6 w-6 rounded-full flex items-center justify-center ${
                  activeTab === 1 ? 'bg-gray-400' : 'bg-indigo-950'
                }`}
              >
                <BiChevronLeft className='text-white' />
              </button>
              <button
                onClick={(e) => handleClick(e)}
                className={`next h-6 w-6 rounded-full flex items-center justify-center ${
                  activeTab === 3 ? 'bg-gray-400' : 'bg-indigo-950'
                }`}
              >
                <BiChevronRight className='text-white' />
              </button>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};
