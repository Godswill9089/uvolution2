import React, { Fragment } from 'react'
import { Navbar } from '../../Navbar/Navbar'
import { Footer } from '../../Footer/Footer';
import "../Training/Training.css"
import "./Source.css";

export const Source = () => {
  return (
    <Fragment>
      <Navbar />
      <section id='home' className='py5'>
        <div className='container flex flex-wrap items-center justify-center mx-auto mt-10  md:px-12 md:flex-row'>
          <div className='mb-14 lg:mb-0 lg:w-1/2'>
            <h1
              className='max-w-xl text-[2.9rem] leading-none font-extrabold text-center lg:text-4xl
                lg:text-left lg:leading-tight mb-5 w-4/5 train-h1'
            >
              Redefining Efficiency and{' '}
              <span className='text-col'>Innovation.</span>
            </h1>
            <p className='max-w-xl text-center lg:text-left lg:max-w-md train-p'>
              At UVTech, we empower businesses like yours to excel by delegating
              critical tasks to our skilled professionals. Our outsourcing
              solutions are designed to streamline your operations, boost
              productivity, and give you the competitive edge needed to thrive
              in today's fast-paced world.
            </p>
            <div className='justify-center mt-14 lg:justify-start'>
              <button
                type='button'
                className='text-white font-medium rounded-lg px-5 py-3 text-center btn-col sou-btn'
              >
                Apply Now
              </button>
            </div>
          </div>
          <div className='lg:w-1/2'>
            <img src='/Img/Group 32.png' alt='' className='training-img' />
          </div>
          <div className='blur1'></div>
          <div className='blur2'></div>
          <div className='blur3'></div>
        </div>
      </section>

      <section id='home1' className='py5 mt-48'>
        <div className='container flex flex-wrap items-center justify-center mx-auto mt-10  md:px-12 md:flex-row'>
          <div className=' lg:w-1/2'>
            <img src='/Img/Group 33.png' alt='' className='lg:w-4/5' />
          </div>
          <div className='lg:w-1/2'>
            <div>
              <img src='/Img/Shape Design (1).png' alt='' className='puzzel' />
            </div>
            <p>UVTECH Outsourcing</p>
            <h1
              className='max-w-xl text-[2.9rem] leading-none font-extrabold text-center lg:text-4xl
                lg:text-left lg:leading-tight mb-5 w-4/5'
            >
              Why Choose UVTech Outsourcing
            </h1>
            <p className='leading-9'>
              Maximize efficiency and unleash innovation by leveraging our
              outsourcing solutions. We connect businesses with top-notch
              technology professionals who possess the skills and dedication to
              drive your projects to success. Let us be your strategic partner
              in achieving your digital objectives
            </p>
          </div>
        </div>
      </section>

      <section id='home2' className='py5 mt-48'>
        <div className='container flex flex-wrap items-center justify-center mx-auto mt-10  md:px-12 md:flex-row'>
          <div className='lg:w-1/2'>
            <div>
              <img src='/Img/Shape Design (1).png' alt='' className='puzzel' />
            </div>
            <p className='train-sub-p'>UVTECH Outsourcing</p>
            <h1
              className='max-w-xl text-[2.9rem] leading-none font-extrabold text-center lg:text-4xl
                lg:text-left lg:leading-tight mb-5 w-4/5 trainn'
            >
              Why Choose UVTech Outsourcing
            </h1>
            <p className='leading-9 tl'>
              Maximize efficiency and unleash innovation by leveraging our
              outsourcing solutions. We connect businesses with top-notch
              technology professionals who possess the skills and dedication to
              drive your projects to success. Let us be your strategic partner
              in achieving your digital objectives
            </p>
          </div>
          <div className=' lg:w-1/2'>
            <img
              src='/Img/Group 33.png'
              alt=''
              className='lg:w-4/5 train-img'
            />
          </div>
        </div>
      </section>

      <div className='lg:w-1/2'>
        <img src='/Img/Shape Design (1).png' alt='' className='puzzel2' />
      </div>
      <div className='g-bg w-full'>
        <div className='bg mt-28 '>
          <p className='text-center justify-center train-sub-p'>Our Services</p>
          <h1 className='text-center trainn text-[2.9rem] font-extrabold lg:text-4xl mb-5'>
            Our Outsourcing Services
          </h1>
        </div>

        <div className='container flex train-le flex-wrap items-center justify-center mx-auto mt-10  md:px-12 md:flex-row justify-between'>
          <div className='box1-bg p-10'>
            <div className='flex'>
              <div className='box1 mb-5'></div>
            </div>
            <h2 className='mb-8 font-extrabold'>IT Outsourcing</h2>
            <p>
              Stay ahead in the ever-evolving technological landscape with
              Uvolution's IT outsourcing services. Our seasoned IT professionals
              offer software development e.g
            </p>
          </div>

          <div className='box2-bg p-10'>
            <div className='flex md:items-center justify-between'>
              <div className='box2 mb-5'></div>
            </div>
            <h2 className='mb-8 font-extrabold'>
              Digital Marketing Outsourcing
            </h2>
            <p>
              Enhance your online presence and maximize your brand's reach with
              Uvolution's digital marketing outsourcing services
            </p>
          </div>

          <div className='box3-bg p-10'>
            <div className='flex md:items-center justify-between'>
              <div className='box3 mb-5'></div>
            </div>
            <h2 className='mb-8 font-extrabold'>Administrative Outsourcing</h2>
            <p>
              Streamline your administrative tasks and improve operational
              efficiency with our administrative outsourcing services.
            </p>
          </div>
        </div>
      </div>

      <section id='home' className='py5 mt-40 train-top'>
        <div className='relative'>
          <div className='container flex train-le flex-wrap items-center justify-center mx-auto mt-10 ml-20  md:px-12 md:flex-row'>
            <div className='blur1'></div>
            <div className=' lg:w-1/2'>
              <img
                src='/Img/Group 34.png'
                alt=''
                className='lg:w-4/5 train-im'
              />
            </div>
            <div className='lg:w-1/2'>
              <div>
                <img src='/Img/Shape Design (1).png' alt='' className='puzze' />
              </div>
              <p className='train-app'>Application</p>
              <h1
                className='max-w-xl text-[2.9rem] leading-none font-extrabold text-center lg:text-4xl
                lg:text-left lg:leading-tight mb-5 train-app'
              >
                Apply Now
              </h1>
              <p className='leading-9 tp'>
                Are you ready to take your career to the next level? Are you
                eager to join a dynamic and innovative team that thrives on
                pushing boundaries and delivering exceptional results? If so
              </p>
              <div className='justify-center mt-14 lg:justify-start'>
                <button
                  type='button'
                  className='text-white font-medium rounded-lg px-5 py-3 text-center btn-col sou-btn'
                >
                  Apply Now
                </button>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </Fragment>
  );
}
