import React, { Fragment } from 'react';
import { Navbar } from '../../Navbar/Navbar';
import './Training.css';
import { BiChevronRight } from 'react-icons/bi';
import { Footer } from '../../Footer/Footer';

export const Training = () => {
  return (
    <Fragment>
      <Navbar />
      <section id='home' className='py5'>
        <div className='container flex flex-wrap items-center justify-center mx-auto mt-10  md:px-12 md:flex-row'>
          <div className='mb-14 lg:mb-0 lg:w-1/2'>
            <h1
              className='max-w-xl text-[2.9rem] leading-none font-extrabold text-center lg:text-4xl
                lg:text-left lg:leading-tight mb-5 w-4/5 train-h1'
            >
              Realise Your Goals with Reality{' '}
              <span className='text-col'>Performance.</span>
            </h1>
            <p className='max-w-xl text-center lg:text-left lg:max-w-md train-p'>
              Unlock your potential with our wide range of technology skill
              training programs. We offer expert-led courses in various domains,
              tailored to both beginners and intermediate learners.
            </p>
            <div className='justify-center mt-14 lg:justify-start'>
              <button
                type='button'
                className='text-white font-medium rounded-lg px-5 py-3 text-center btn-col train-btn'
              >
                Apply Now
              </button>
            </div>
          </div>
          <div className='lg:w-1/2'>
            <img
              src='/Img/Training Asset.png'
              alt=''
              className='training-img'
            />
          </div>
          <div className='blur1'></div>
          <div className='blur2'></div>
          <div className='blur3'></div>
        </div>
      </section>

      <section id='home1' className='py5 mt-48'>
        <div className='container flex flex-wrap items-center justify-center mx-auto mt-10  md:px-12 md:flex-row'>
          <div className=' lg:w-1/2'>
            <img src='/Img/Group 29.png' alt='' className='lg:w-4/5' />
          </div>
          <div className='lg:w-1/2'>
            <div>
              <img src='/Img/Shape Design (1).png' alt='' className='puzzel' />
            </div>
            <p>UVTECH TRAINING</p>
            <h1
              className='max-w-xl text-[2.9rem] leading-none font-extrabold text-center lg:text-4xl
                lg:text-left lg:leading-tight mb-5 w-4/5'
            >
              Why Choose UVTech Training
            </h1>
            <p className='leading-9'>
              At UVTech we believe that investing in your team's professional
              development is the key to unlocking your organisations true
              potential. Our comprehensive training programs are designed to
              equip your employees with the knowledge, skills, and confidence
              they need to excel in their roles and drive your business towards
              unprecedented success.
            </p>
          </div>
        </div>
      </section>

      <section id='home2' className='py5 mt-48'>
        <div className='container flex flex-wrap items-center justify-center mx-auto mt-10  md:px-12 md:flex-row'>
          <div className='lg:w-1/2'>
            <div>
              <img src='/Img/Shape Design (1).png' alt='' className='puzzel' />
            </div>
            <p className='train-sub-p'>UVTECH TRAINING</p>
            <h1
              className='max-w-xl text-[2.9rem] leading-none font-extrabold text-center lg:text-4xl
                lg:text-left lg:leading-tight mb-5 w-4/5 trainn'
            >
              Why Choose UVTech Training
            </h1>
            <p className='leading-9 tl'>
              At UVTech we believe that investing in your team's professional
              development is the key to unlocking your organisations true
              potential. Our comprehensive training programs are designed to
              equip your employees with the knowledge, skills, and confidence
              they need to excel in their roles and drive your business towards
              unprecedented success.
            </p>
          </div>
          <div className=' lg:w-1/2'>
            <img
              src='/Img/Group 29.png'
              alt=''
              className='lg:w-4/5 train-img'
            />
          </div>
        </div>
      </section>

      <div className='lg:w-1/2'>
        <img src='/Img/Shape Design (1).png' alt='' className='puzzel2' />
      </div>
      <div className='g-bg w-full'>
        <div className='bg mt-28 '>
          <p className='text-center train-sub-p justify-center'>
            UVTECH Course
          </p>
          <h1 className='text-center  text-[2.9rem] font-extrabold lg:text-4xl mb-5 trainn'>
            Our Training Programs
          </h1>
        </div>

        <div className='container train-box flex flex-wrap items-center justify-center mx-auto mt-10  md:px-12 md:flex-row justify-between'>
          <div className='box1-bg p-10'>
            <div className='flex '>
              <div className='box1 mb-5'></div>
            </div>
            <h2 className='mb-2 font-extrabold'>Project Management</h2>
            <p>
              An opportunity to be mentored by seasoned project managers who
              will guide and support you on your journey.
            </p>
            <a href='/projects' className='flex items-center mt-20'>
              Learn More <BiChevronRight className='items-center mt-1' />
            </a>
          </div>

          <div className='box2-bg p-10'>
            <div className='flex md:items-center justify-between'>
              <div className='box2 mb-5'></div>
              <p className='font-extralight'>Coming Soon...</p>
            </div>
            <h2 className='mb-2 font-extrabold'>Business Analysis</h2>
            <p>
              An opportunity to be mentored by seasoned project managers who
              will guide and support you on your journey.
            </p>
            <a href='!#' className='flex items-center mt-20 font-extralight'>
              Learn More <BiChevronRight className='items-center mt-1' />
            </a>
          </div>

          <div className='box3-bg p-10'>
            <div className='flex md:items-center justify-between'>
              <div className='box3 mb-5'></div>
              <p className='font-extralight'>Coming Soon...</p>
            </div>
            <h2 className='mb-2 font-extrabold'>Data Analysis</h2>
            <p>
              An opportunity to be mentored by seasoned project managers who
              will guide and support you on your journey.
            </p>
            <a href='!#' className='flex items-center mt-20 font-extralight'>
              Learn More <BiChevronRight className='items-center mt-1' />
            </a>
          </div>
        </div>
      </div>

      <section id='home' className='py5 mt-40 train-top'>
        <div className='relative'>
          <div className='container flex train-le flex-wrap items-center justify-center mx-auto mt-10 ml-20  md:px-12 md:flex-row'>
            <div className='blur1'></div>
            <div className=' lg:w-1/2'>
              <img
                src='/Img/Group 31.png'
                alt=''
                className='lg:w-4/5 train-im'
              />
            </div>
            <div className='lg:w-1/2'>
              <div>
                <img src='/Img/Shape Design (1).png' alt='' className='puzze' />
              </div>
              <p className='train-app'>Application</p>
              <h1
                className='max-w-xl text-[2.9rem] leading-none font-extrabold text-center lg:text-4xl
                lg:text-left lg:leading-tight mb-5 train-app '
              >
                Apply Now
              </h1>
              <p className='leading-9 tp'>
                Are you ready to take your career to the next level? Are you
                eager to join a dynamic and innovative team that thrives on
                pushing boundaries and delivering exceptional results? If so
              </p>
              <div className='justify-center mt-14 lg:justify-start'>
                <button
                  type='button'
                  className='text-white font-medium rounded-lg px-5 py-3 text-center btn-col train-btn'
                >
                  Apply Now
                </button>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </Fragment>
  );
};
