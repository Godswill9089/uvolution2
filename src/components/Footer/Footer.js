import React, { Fragment } from 'react'
import './Footer.css';

export const Footer = () => {
  return (
    <Fragment>
      <div className='container flex footer flex-wrap items-center justify-center justify-between mx-auto mt-40 mb-10 ml-20  md:px-12 md:flex-row'>
        <img src='/Img/UV TECH LOGO 1.png' alt='logo' className='' />
        <p>Copyright 2023.. Powered By Uvolution Technology</p>
      </div>
    </Fragment>
  );
}
