import React, { Fragment } from 'react'
import "./Application.css";

export const Application = () => {
  return (
    <Fragment>
      <section id='home' className='py5 mt-20'>
        <div className='relative'>
          <div className='container flex flex-wrap items-center justify-center mx-auto mt-10 ml-20 app-con md:px-12 md:flex-row'>
            <div className='blur1'></div>
            <div className=' lg:w-1/2'>
              <img src='/Img/Apply Asset.png' alt='' className='lg:w-4/5 app-set' />
            </div>
            <div className='lg:w-1/2'>
              <div>
                <img src='/Img/Shape Design (1).png' alt='' className='puzze' />
              </div>
              <p className='app-p'>Application</p>
              <h1
                className='max-w-xl text-[2.9rem] leading-none font-extrabold text-center lg:text-4xl
                lg:text-left lg:leading-tight mb-5 app-h1'
              >
                Apply Now
              </h1>
              <p className='leading-9 app-text'>
                Are you ready to take your career to the next level? Are you
                eager to join a dynamic and innovative team that thrives on
                pushing boundaries and delivering exceptional results? If so
              </p>
              <div className='justify-center mt-14 lg:justify-start'>
                <button
                  type='button'
                  className='text-white font-medium rounded-lg px-5 py-3 text-center btn-col app-btn'
                >
                  Apply Now
                </button>
              </div>
            </div>
          </div>
        </div>
      </section>
    </Fragment>
  );
}
