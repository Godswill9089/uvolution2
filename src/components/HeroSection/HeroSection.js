import React, { Fragment } from 'react';
import "./HeroSection.css";

export const HeroSection = () => {
  return (
    <Fragment>
      <section id='home' className='py5'>
        <div className='container flex flex-wrap items-center justify-center mx-auto mt-10  md:px-12 md:flex-row'>
          <div className='mb-14 lg:mb-0 lg:w-1/2'>
            <h1
              className='max-w-xl text-[2.9rem] leading-none font-extrabold text-center lg:text-4xl
                lg:text-left lg:leading-tight mb-5 hero-h1'
            >
              Empowering Skills.Enabling{' '}
              <span className='text-col'>Innovation.</span> Embrace the Future
              with Us
            </h1>
            <p className='max-w-xl text-center lg:text-left lg:max-w-md p-h1'>
              We are a transformative force, empowering individuals and
              businesses to reach new heights.
            </p>
            <div className='justify-center mt-14 lg:justify-start'>
              <button
                type='button'
                className='text-white font-medium rounded-lg px-5 py-3 text-center btn-col hero-btn'
              >
                Apply Now
              </button>
            </div>
          </div>
          <div className='lg:w-1/2'>
            <img src='/Img/Hero Asset.png' alt='' className='hero-img'/>
          </div>
          <div className='blur1'></div>
          <div className='blur2'></div>
          <div className='blur3'></div>
        </div>
      </section>
    </Fragment>
  );
};
