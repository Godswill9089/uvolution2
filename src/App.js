import React  from 'react'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { Home } from './Home';
import { Training } from './components/Pages/Training/Training';
import { Source } from './components/Pages/Source/Source';
import { Project } from './components/Project/Project';


function App() {
  return (
    <Router>
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/training' element={<Training />} />
        <Route path='/source' element={<Source />} />
        <Route path='/projects' element={<Project />} />
      </Routes>
    </Router>
  );
}

export default App;
