import React from 'react';
import { AboutSection } from './components/AboutSection/AboutSection';
import { Application } from './components/Application/Application';
import { Footer } from './components/Footer/Footer';
import { HeroSection } from './components/HeroSection/HeroSection';
import { Navbar } from './components/Navbar/Navbar';
import { Question } from './components/Question/Question';
import { Services } from './components/ServicesSection/Services';
import { Testimonial } from './components/Testimonials/Testimonial';

export const Home = () => {
  return (
    <div className=''>
      <Navbar />
      <HeroSection />
      <AboutSection />
      <Services />
      <Testimonial />
      <Question />
      <Application />
      <Footer />
    </div>
  );
};
